#from server import Server
from srt import Server
import os
import django
import schedule
os.environ["DJANGO_SETTINGS_MODULE"] = 'test.settings'
django.setup()
from nodes.models import NodePing, Node


# format network message
# Add = <1>:<ip>:<Wst>
# Delete = <2>:<ip>:<Wst>
# Reload = <3>
# Exit = <0>

class t1(Server):
    def handle(self, message):
#        global nodes
        try:
            m = message.decode('utf-8').rstrip()
#            print("Got: {}".format(m[0]))
#            print(type(m))
            if m[0] == "0":
                print("Stop server")
                self.stop_server()
                exit(0)

            if m[0] == "1":
                print("add ip to ping")
                cmd, ip, wst = m.split(":")
                Server.nodes.append ({'Ip': ip, 'Wst': int(wst)})
                print(Server.nodes)
                # add tag's job
                schedule.every(int(wst)).seconds.do(self.run_threaded, self.job, [ip]).tag(ip)
                schedule.run_pending()
                # add node to shedule list

            if m[0] == "2":
                print("delete ip from ping")
                cmd, ip, wst = m.split(":")
                n = {'Ip': ip, 'Wst': int(wst)}
                print(n)
                t1.nodes.remove(n)
                print(Server.nodes)
                # remove tag's job
                schedule.clear(tag=ip)
                # add remove node from shedule list

            if m[0] == "3":
                print("reload ip from DB")
                self.queue.clear()
                Server.nodes = list(Node.objects.all().filter(enabled=True)
                                                      .values('Ip', 'Wst'))
                Server.cmd_srv = 3
                print(Server.nodes)
        except Exception as e:
            print("Error: {}".format(e))


if __name__ == "__main__":
    print("T1 started.")
#    nodes = list(Node.objects.all().filter(enabled=True)
#                                   .values('Ip', 'Wst'))
    app = t1("0.0.0.0", 8889)
    try:
      Server.nodes = list(Node.objects.all().filter(enabled=True)
                                   .values('Ip', 'Wst'))
    except  Exception as e:
      Server.nodes = list()
    app.start_server()
    app.loop()
    app.stop_server()
