import Vue from "vue";
import VueRouter from "vue-router";
import RouterPrefetch from 'vue-router-prefetch'
import App from "./App";
// TIP: change to import router from "./router/starterRouter"; to start with a clean layout
import router from "./router/index";

import BlackDashboard from "./plugins/blackDashboard";
import i18n from "./i18n"
import './registerServiceWorker'
Vue.use(BlackDashboard);
Vue.use(VueRouter);
Vue.use(RouterPrefetch);
import store from './store'
import Axios from 'axios'

/* eslint-disable no-new */
Vue.prototype.$http = Axios
const b_token = localStorage.getItem('Bearer')
if (b_token) {
  Axios.defaults.headers.common['Authorization'] = "Bearer " + b_token
}

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
