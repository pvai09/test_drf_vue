import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    status_saving:"",
    status_delete: '',
    srv_alive: '',
    srv_status: false,
    b_token: localStorage.getItem('Bearer') || '',
    r_token: localStorage.getItem('Refresh') || '',
    user : {
      fullName: '123',
      title: '123',
      description: '123',
    },
    model: {
      company: '',
      email: '',
      username: '',
      first_name: '',
      last_name: '',
      address: '',
      city: '',
      country: '',
      about: '',
      post_code: ''
    },
    node:{
      Ip: "",
      Wst:"",
      Wpc:"",
      enabled:"True",
      groups:{}
    },
    nodes:{
    },
    pings:[],
    db_ip:"",
    groups:{
    },
    group:{
    },
    selected_groups:{
    },
    isError :'',
    chart_x : [],
    chart_y : [],
    last_data :[{Ip: "",
      nodepings: {
       NodeId: "", TimeBegin: "", TimeEnd: "", max_ping: "", mdev: "",
       mid_ping: "", min_ping: "", ploss: ""
     },
   }]
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, b_token, r_token){
      state.status = 'success'
      state.b_token = b_token
      state.r_token = r_token
    },
    user_loading(state){
      state.status = 'loading'
    },
    nodes_loading(state){
      state.status = 'loading'
    },
    nodes_loaded(state, nodes){
      state.status = 'loaded'
      state.nodes = nodes
    },
    pings_loading(state){
      state.status = 'loading'
    },
    pings_loaded(state, pings){
      state.status = 'loaded'
      state.pings = pings
    },
    node_saving(state){
      state.status_saving = 'saving'
    },
    node_deleting(state, node){
      state.status_delete = 'deleting'
      state.node = node
    },
    node_deleted(state, node){
      state.status_delete = 'deleted'
      var filtered = state.nodes.filter(function(value, index, arr){ return value !== node;})
      state.nodes = filtered
    },
    node_saved(state, node){
      state.status_saving = 'saved'
      state.nodes.push(node)
// clear current node values
      state.node = {}
//      console.log(state.nodes)
    },
    node_new(state, node){
      state.node = node
    },
    groups_loading(state){
      state.status = 'loading'
    },
    groups_loaded(state, groups){
      state.status = 'loaded'
      state.groups = groups
    },
    group_saved(state, group){
      state.group = group
    },
    group_saving(state){

    },
    group_deleted(state, group){
      state.group = {}
    },
    group_deleting(state){

    },
    srv_alive(state, last_resp_time){
      state.srv_alive = last_resp_time
    },
    last_data(state, data){
      state.last_data = data
    },
    user_loaded(state, user){
//      state.user = user
      state.model = {}
      state.status = 'loaded'
      state.model.email = user.email
      state.model.first_name = user.first_name
      state.model.last_name = user.last_name
      state.user.fullName = user.first_name + " " + user.last_name
      state.user.description = user.about
      state.user.title = user.username
      state.model.company = user.company
      state.model.username = user.username
      state.model.address = user.address
      state.model.city = user.city
      state.model.country = user.country
      state.model.about = user.about
      state.model.post_code = user.post_code
    },
    auth_error(state, err){
      state.status = 'error'
      state.isError= err
    },
    logout(state){
      state.status = ''
      state.b_token = ''
      state.r_token = ''
      state.isError=''
      state.user={}
      state = {}
    },
    updateModelAbout(about){
      state.model.about = about
      state.user.description = about
    },
    updateModelPostCode(post_code){
      state.model.post_code = post_code
    },
    updateModelCompany(company){
      state.model.company = company
    },
    updateModelUsername(username){
      state.model.username = username
      state.user.title = username
    },
    updateModelEmail(email){
      state.model.email = email
    },
    updateModelFirstName(firstName){
      state.model.first_name = firstName
      state.user.fullName = firstName + " " + state.model.last_name
    },
    updateModelLastName(lastName){
      state.model.last_name = lastName
      state.user.fullName = state.model.first_name + " " + lastName
    },
    updateModelAddress(address){
      state.model.address = address
    },
    updateModelCity(city){
      state.model.city = city
    },
    updateModelCountry(country){
      state.model.country = country
    },
    updateModel(model){
      state.model = model
    }
  },
  actions: {
    login({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: 'http://10.92.0.13:8000/auth/jwt/create/', data: user, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          const refresh_token = resp.data.refresh
//          const user = resp.data.user
          localStorage.setItem('Bearer', bearer_token)
          localStorage.setItem('Refresh', refresh_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
          commit('auth_success', bearer_token, refresh_token)
//          console.log(resp.data)
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('Bearer', 'Refresh')
          reject(err)
        })
      })
    },

    load_user({commit}, user){
      return new Promise((resolve, reject) => {
        commit('user_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
//          axios({url: 'http://10.92.0.13:8000/auth/users/me/', method: 'GET' })
          axios({url: 'http://10.92.0.13:8000/user/info/', method: 'GET' })
         .then(resp1 => {
            commit('user_loaded',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('user_loading',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },
    update_user({commit}, user){
      return new Promise((resolve, reject) => {
        commit('user_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
//          axios({url: 'http://10.92.0.13:8000/auth/users/me/', method: 'GET' })
          axios({url: 'http://10.92.0.13:8000/user/update/', data: {user: user}, method: 'PUT' })
         .then(resp1 => {
            commit('user_loaded',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('user_loading',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },
    load_nodes({commit}, nodes){
      return new Promise((resolve, reject) => {
        commit('user_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/all/', method: 'GET' })
         .then(resp1 => {
            commit('nodes_loaded',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('nodes_loading',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },

    save_node({commit}, node){
      return new Promise((resolve, reject) => {
        commit('node_saving')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/create/', data: node, method: 'POST' })
         .then(resp1 => {
            commit('node_saved',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('node_saving',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },

    delete_node({commit}, node){
      return new Promise((resolve, reject) => {
        commit('node_deleting', node)
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
//          console.log("Node = " + node)
          axios({url: 'http://10.92.0.13:8000/node/delete/', data: node, method: 'DELETE' })
         .then(resp1 => {
            commit('node_deleted', node)
            resolve(resp1)
          }).
          catch(err => {
             commit('node_deleting',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },

    load_groups({commit}, groups){
      return new Promise((resolve, reject) => {
        commit('groups_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/group/all/', method: 'GET' })
         .then(resp1 => {
//            console.log(resp1.data)
            commit('groups_loaded',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('groups_loading',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },
    group_save({commit}, group){
      return new Promise((resolve, reject) => {
        commit('groups_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/group/save/', data: group, method: 'POST' })
         .then(resp1 => {
            console.log(resp1.data)
            commit('group_saved',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('group_saving',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },
    group_delete({commit}, group){
      return new Promise((resolve, reject) => {
        commit('groups_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/group/delete/', data: group, method: 'DELETE' })
         .then(resp1 => {
            console.log(resp1.data)
            commit('group_deleted',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('group_deleting',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },

    load_pings({commit}, node_ip){
      return new Promise((resolve, reject) => {
        commit('user_loading')
// обновляем токен
        axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
        .then(resp => {
          const bearer_token = resp.data.access
          localStorage.setItem('Bearer', bearer_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
          axios({url: 'http://10.92.0.13:8000/node/get_pings/', data:{Ip: node_ip},  method: 'POST' })
         .then(resp1 => {
            commit('pings_loaded',resp1.data)
            resolve(resp1)
          }).
          catch(err => {
             commit('pings_loading',err)
             reject(err)
          })
         })
         .catch(err => {
           commit('auth_error')
//           console.log(err)
         })
      })
    },

   register({commit}, user){
    return new Promise((resolve, reject) => {
      commit('auth_request')
      axios({url: 'http://10.92.0.13:8000/auth/users/', data: user, method: 'POST' })
      .then(resp => {
        resolve(resp)
      })
      .catch(err => {
        commit('auth_error', err.response.data)
        console.log(err.response.data)
        localStorage.removeItem('Bearer', 'Refresh')
        reject(err)
      })
    })
  },

  srv_post_cmd({commit}, cmd){
    return new Promise((resolve, reject) => {
//      commit('auth_request')
      axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
      .then(resp => {
        const bearer_token = resp.data.access
        localStorage.setItem('Bearer', bearer_token)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию
        axios({url: 'http://10.92.0.13:8000/node/srv/', data:{cmd: cmd},  method: 'POST' })
       .then(resp1 => {
//          commit('pings_loaded',resp1.data)
          resolve(resp1)
        }).
        catch(err => {
//           commit('pings_loading',err)
           reject(err)
        })
       })
       .catch(err => {
         commit('auth_error')
//         console.log(err)
       })
     })
  },

  srv_get_alive({commit} ){
    return new Promise((resolve, reject) => {
//      commit('auth_request')
      axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
      .then(resp => {
        const bearer_token = resp.data.access
        localStorage.setItem('Bearer', bearer_token)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token

// Запрашиваем информацию о сервере
          axios({url: 'http://10.92.0.13:8000/node/srv/srv_alive/', method: 'Get' })
         .then(resp1 => {
            commit('srv_alive',resp1.data)
            resolve(resp1.data)
          }).
          catch(err => {
  //           commit('pings_loading',err)
             reject(err)
          })
// Запрашиваем информацию о последних данных
          axios({url: 'http://10.92.0.13:8000/node/last_data/', method: 'Get' })
          .then(resp2 => {
            commit('last_data',resp2.data)
            resolve(resp2.data)
          }).
          catch(err => {
          //           commit('pings_loading',err)
             reject(err)
          })
       })
       .catch(err => {
         commit('auth_error')
//         console.log(err.response.status)
         if (err.response.status === 401) {
           router.push('/')
         }
       })
      })
  },

  srv_get_last_data({commit} ){
    return new Promise((resolve, reject) => {
//      commit('auth_request')
      axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', data:{refresh : localStorage.getItem('Refresh')}, method: 'POST' })
      .then(resp => {
        const bearer_token = resp.data.access
        localStorage.setItem('Bearer', bearer_token)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
// Запрашиваем информацию о последних данных
          axios({url: 'http://10.92.0.13:8000/node/last_data/', method: 'Get' })
          .then(resp2 => {
            commit('last_data',resp2.data)
            resolve(resp2.data)
          }).
          catch(err => {
          //           commit('pings_loading',err)
             reject(err)
          })
       })
       .catch(err => {
         commit('auth_error')
         if (err.response.status === 401) {
           router.push('/')
         }
       })
      })
  },
   logout({commit}){
    return new Promise((resolve, reject) => {
      commit('logout')
//      localStorage.removeItem('token')
      localStorage.removeItem('Bearer')
      localStorage.removeItem('Refresh')
      delete axios.defaults.headers.common['Authorization']
      resolve()
    })
  }
  },
  modules: {
  },
  getters : {
      isLoggedIn: state => !!state.b_token,
      authStatus: state => state.status,
      isError: state => state.isError,
      user: state => state.user,
      model: state => state.model
  }
})
