import axios from 'axios'

refresh_t(){
  return new Promise((resolve, reject) => {
    commit('auth_request')
    axios({url: 'http://10.92.0.13:8000/auth/jwt/refresh/', method: 'POST' })
    .then(resp => {
      const bearer_token = resp.data.access
      const refresh_token = resp.data.refresh
//          const user = resp.data.user
      localStorage.setItem('Bearer', bearer_token)
      localStorage.setItem('Refresh', refresh_token)
      axios.defaults.headers.common['Authorization'] = 'Bearer '+ bearer_token
      commit('auth_success', bearer_token, refresh_token)
//          console.log(resp.data)
      resolve(resp)
    })
    .catch(err => {
      commit('auth_error')
      localStorage.removeItem('Bearer', 'Refresh')
      reject(err)
    })
  })
};
