import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";
import Login from "@/pages/Login.vue";
import store from '@/store/index.js';

// Admin pages
const Dashboard = () => import(/* webpackChunkName: "dashboard" */"@/pages/Dashboard.vue");
const Profile = () => import(/* webpackChunkName: "common" */ "@/pages/Profile.vue");
const Notifications = () => import(/* webpackChunkName: "common" */"@/pages/Notifications.vue");
//const Icons = () => import(/* webpackChunkName: "common" */ "@/pages/Icons.vue");
const Maps = () => import(/* webpackChunkName: "common" */ "@/pages/Maps.vue");
//const Typography = () => import(/* webpackChunkName: "common" */ "@/pages/Typography.vue");
const TableList = () => import(/* webpackChunkName: "common" */ "@/pages/TableList.vue");
const NodeGroups = () => import(/* webpackChunkName: "common" */ "@/pages/Nodesgroups.vue");
const Nodes = () => import(/* webpackChunkName: "common" */ "@/pages/Nodes.vue");
const Map = () => import(/* webpackChunkName: "common" */ "@/pages/Map.vue");
const reg = () => import(/* webpackChunkName: "common" */ "@/pages/Register.vue");

const routes = [
  {
   path: "/",
   name: 'login',
   component: Login
  },
  {
   path: "/reg",
   name: 'reg',
   component: reg
  },
  {
    path: "/home",
    name: 'home',
    component: DashboardLayout,
    meta: {
       requiresAuth: true
    },
    beforeEnter: (to, from, next) => {
      if (store.getters.isLoggedIn) {
        next()
        return
      }
      next('/')
    },
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },

      {
        path: "profile",
        name: "profile",
        component: Profile,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },
/*      {
        path: "icons",
        name: "icons",
        component: Icons
      },

      {
        path: "maps",
        name: "maps",
        component: Maps
      },
*/
      {
        path: "table-list",
        name: "table-list",
        component: TableList,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },
      {
        path: "nodes",
        name: "nodes",
        component: Nodes,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },
      {
        path: "node-groups",
        name: "node-groups",
        component: NodeGroups,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },
      {
        path: "map",
        name: "map",
        component: Map,
        meta: {
           requiresAuth: true
        },
        beforeEnter: (to, from, next) => {
          if (store.getters.isLoggedIn) {
            next()
            return
          }
          next('/')
        },
      },

      {
        path: "logout",
        name: "logout",
        redirect: '/',
      }
    ]
  },
  { path: "*", component: NotFound },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
