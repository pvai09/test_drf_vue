import VueRouter from "vue-router";
import routes from "./routes";
import store from "@/store";

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: "active",
  beforeEach: (to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isLoggedIn) {
        next()
        return
      }
      next('/')
    } else {
      next()
    }
  },
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
});
//console.log(router.beforeEach)



export default router;
