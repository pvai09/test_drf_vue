import socket
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework_jwt.serializers import JSONWebTokenSerializer, \
                                            jwt_payload_handler
from .models import Node, NodeGroup, NodePing, NodePingSrv
from users.models import User
from nodes.serializers import NodeSerializer, NodeGroupSerializer,\
                              NodePingSerializer, NodePingSrvSerializer,\
                              NodeLastPingSerializer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
import jwt
import logging
from datetime import datetime
from .settings import SRV_IP, SRV_port
#from rest_framework.pagination import CursorPagination

# Create your views here.
@permission_classes([IsAuthenticated, ])
class CreateNodeAPIView(APIView) : # создание нового узла
    serializer_class = NodeSerializer

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        try:
            node = Node.objects.get(Ip=data.get('Ip'))
        except:
            node = None
        if node:
          # update exist node
          node_enabled_old = node.enabled
          serializer = self.serializer_class(node, data = data)
          if serializer.is_valid():
            print('-----------Update-------------')
            serializer.save()
            # check node enabled field and add to ping srv if (false -> true)
            if not node_enabled_old:
                if node.enabled:
                    print("### Add node to ping srv ###")
                    cmd = "1" + ":" + node.Ip + ":" + str(node.Wst)
                    print("send to server {}:{}, cmd:{}".format(SRV_IP, SRV_port, cmd))
                    try:
                        send(self, SRV_IP, int(SRV_port), cmd.encode())
                    except Exception as e:
                        "Error: {}".format(e)
            if node_enabled_old:
                if not node.enabled:
                    print("### Delete node from ping srv ###")
                    cmd = "2" + ":" + node.Ip + ":" + str(node.Wst)
                    print("send to server {}:{}, cmd:{}".format(SRV_IP, SRV_port, cmd))
                    try:
                        send(self, SRV_IP, int(SRV_port), cmd.encode())
                    except Exception as e:
                        "Error: {}".format(e)
                return JsonResponse(serializer.data, status=status.HTTP_200_OK)
            else:
                return JsonResponse(serializer.errors, status=400)
        else:
          # create new node
            serializer = self.serializer_class(data = data)
            if serializer.is_valid():
                print('-----------Save-------------')
                print("### IP = " + data.get('Ip'))
                serializer.save()
                if data.get('enabled') == True:
                    print("### Add node to ping srv ")
                    cmd = "1" + ":" + data.get('Ip') + ":" + str(data.get('Wst'))
                    print("send to server {}:{}, cmd:{}".format(SRV_IP, SRV_port, cmd))
                    try:
                        send(self, SRV_IP, int(SRV_port), cmd.encode())
                    except Exception as e:
                        "Error: {}".format(e)
                return JsonResponse(serializer.data, status=status.HTTP_200_OK)
            else:
                return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes([IsAuthenticated, ])
class NodesAPIView(APIView) :   # return all nodes
    serializer_class = NodeSerializer

    def get(self, request, *args, **kwargs):
        all_nodes = Node.objects.all()
        serializer = self.serializer_class(all_nodes, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer_data = request.data.get('node', {})

        serializer = NodeSerializer(
            request.node, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class NodeGroupAPIView(APIView):
    serializer_class = NodeGroupSerializer
    def get(self, request, *args, **kwargs):
        all_groups = NodeGroup.objects.all()
        serializer = self.serializer_class(all_groups, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class SaveNodeGroupAPIView(APIView):
    serializer_class = NodeGroupSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = NodeGroupSerializer()
        try:
            node_grp = NodeGroup.objects.get(GName=data.get('GName'))
        except Exception as e:
            node_grp = None
        if node_grp:
            serializer_data = data  # .get('group', {})
            serializer = NodeGroupSerializer(
                node_grp, data=serializer_data, partial=True
            )
        else:
            # New Group
            serializer_data = data  # .get('group', {})
            ngrp = NodeGroup()
            usr = User.objects.get(login=request.user)
            ngrp.Uid = usr
            serializer = NodeGroupSerializer(
                ngrp, data=serializer_data, partial=True
            )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class DeleteNodeGroupAPIView(APIView):
    serializer_class = NodeGroupSerializer

    def delete(self, request):
        data = request.data
        try:
            ngrp = NodeGroup.objects.get(GName = data.get('GName'))
            ngrp.delete()
            # delete node from ping server< if enabled = True
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)



@permission_classes([IsAuthenticated, ])
class CreateNodeGroupAPIView(APIView):
  pass


@permission_classes([IsAuthenticated, ])
class DeleteNodeAPIView(APIView):
    def delete(self, request):
        data = JSONParser().parse(request)
        try:
            node = Node.objects.get(Ip=data.get('Ip'))
            node.delete()
            # delete node from ping server< if enabled = True
            if node.enabled:
                print("### Delete node from ping srv ###")
                cmd = "2" + ":" + node.Ip + ":" + str(node.Wst)
                print("send to server {}:{}, cmd:{}".format(SRV_IP, SRV_port, cmd))
                try:
                    send(self, SRV_IP, int(SRV_port), cmd.encode())
                except Exception as e:
                    "Error: {}".format(e)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


@permission_classes([IsAuthenticated, ])
class NodesPingAPIView(APIView):
    serializer_class = NodePingSerializer
#    pagination_class = CursorPagination
    def post(self, request, *args, **kwargs):

        data = JSONParser().parse(request)
        try:
            node = Node.objects.get(Ip=data.get('Ip'))
            try:
                node_ping = reversed(list(node.nodeping_set.all().order_by('-id')[:30]))
            except Exception as e:
                node_ping = node.nodeping_set.all()
            serializer = self.serializer_class(node_ping, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@permission_classes([IsAuthenticated, ])
class SrvPostCmdAPIView(APIView):
    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        cmd = data.get('cmd')
        print("send to server {}:{}, cmd:{}".format(SRV_IP, SRV_port, cmd))
        send(self, SRV_IP, int(SRV_port), cmd.encode())
        return Response(status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class SrvGetAliveAPIView(APIView):
    serializer_class = NodePingSrvSerializer
    def get(self, request, *args, **kwargs):
        try:
            n = NodePingSrv.objects.get(ip=SRV_IP)
        except Exception as e:
#            print("Error: {}".format(e))
            n = NodePingSrv(TimeChk = datetime.now(), ip = SRV_IP)
            n.save(force_insert = True)
        serializer = self.serializer_class(n)
        return Response(serializer.data, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class GetLastDataAPIView(APIView):
    serializer_class = NodeLastPingSerializer
    def get(self, request, *args, **kwargs):
        nodes = Node.objects.filter(enabled=True)
        serializer = self.serializer_class(nodes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


def send(self, ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    try:
        sock.sendall(message)
    finally:
        sock.close()
