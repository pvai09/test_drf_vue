from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.conf import settings # for use AUTH_USER_MODEL
from django.contrib.auth.models import (
    PermissionsMixin
)
from django.db import transaction
from .settings import SRV_IP, SRV_port


# Create your models here.

class NodeGroup(models.Model): #, PermissionsMixin):
    GName = models.CharField(max_length=50, default='') # group name
    Uid = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE) # belong_to user Id
    descr = models.CharField(max_length=250, default='') # group description

    def __str__(self):
        return self.GName

class Node(models.Model):

    Ip = models.GenericIPAddressField(protocol='both', unpack_ipv4=False, unique='True') # Ip v6 max = 45
    Wst = models.IntegerField(default=1) # интервал запуска воркера в минутах
    Wpc = models.IntegerField(default=3) # количество пингов
    enabled = models.BooleanField(default=True) # по умолчанию включено
    groups =  models.ManyToManyField(NodeGroup) # узел может быть в нескольких группах


class NodePing(models.Model):
    NodeId = models.ForeignKey(Node, on_delete=models.CASCADE) # внешний ключ = node , удаляется все вместе.
    TimeBegin = models.DateTimeField() #
    TimeEnd = models.DateTimeField()
    min_ping = models.FloatField(default=0.0)
    mid_ping = models.FloatField(default=0.0)
    max_ping = models.FloatField(default=0.0)
    mdev = models.FloatField(default=0.0)
    ploss = models.FloatField(default=0.0)

    def __uncode__(self):
        return '%d: %s' % (self.NodeId, self.mid_ping)


class NodePingSrv(models.Model):
    TimeChk = models.DateTimeField()
    ip = models.GenericIPAddressField(protocol='both', unpack_ipv4=False, default="127.0.0.1")


#    def __str__(self):
#        return (self.NodeId)
