from rest_framework import serializers
from .models import Node, NodeGroup, NodePing, NodePingSrv


class NodeGroupSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = NodeGroup
        fields = ['GName', 'Uid', 'descr',]


class NodeSerializer(serializers.ModelSerializer):
    groups = NodeGroupSerializer(many=True)

    def dp(self, s):
        print('######################')
        print(s)
        print('######################')
        return 0

    class Meta(object):
        model = Node
        fields = ['Ip', 'Wst','Wpc', 'enabled', 'groups',]

    def create(self, validated_data):
        grp = validated_data.pop('groups')
        grps = []
        self.dp(validated_data)
        for g in grp:
          grps.append(NodeGroup.objects.get(GName=g['GName']))
        node = Node.objects.create(**validated_data)
        node.groups.set(grps)
        node.save()
        return node

    def update(self, node, validated_data):
        grp = validated_data.pop('groups')
        ip = validated_data.pop('Ip')
        grps = []
        for g in grp:
          grps.append(NodeGroup.objects.get(GName=g['GName']))
#        node = Node.objects.get(Ip=ip)
        node.Wst = validated_data.pop('Wst')
        node.Wpc = validated_data.pop('Wpc')
        node.enabled = validated_data.pop('enabled')
        node.groups.set(grps)
        node.save()
        return node


class NodePingSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = NodePing
        fields = ('NodeId', 'TimeBegin','TimeEnd', 'min_ping', 'mid_ping', 'max_ping', 'mdev', 'ploss')


class NodeLastPingSerializer(serializers.ModelSerializer):
#    nodepings = NodePingSerializer(many=True, read_only=True) # node.nodeping_set.last()
#    np = serializers.RelatedField(many=True, read_only=True)
#    print(repr(nodepings))
#    class Meta(object):
#        model = Node
#        fields = ('Ip', 'nodepings')
#        read_only_fields = fields
#    nodepings = NodePingSerializer(many=True, read_only=True)
#    nodepings = NodePingSerializer(many=True, read_only=True)
    nodepings = serializers.SerializerMethodField('_get_children')

    def _get_children(self, obj):
        serializer = NodePingSerializer(obj.nodeping_set.all().last())
        return serializer.data

    print(repr(nodepings))
    class Meta(object):
        model = Node
        fields = ['Ip', 'nodepings',]


class NodePingSrvSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = NodePingSrv
        fields = ['TimeChk']
