from django.conf.urls import url #, patterns
from .views import CreateNodeAPIView, NodesAPIView, NodeGroupAPIView,\
                   DeleteNodeAPIView, CreateNodeGroupAPIView, \
                   NodesPingAPIView, SrvPostCmdAPIView, SrvGetAliveAPIView,\
                   GetLastDataAPIView, SaveNodeGroupAPIView, DeleteNodeGroupAPIView


urlpatterns = [
    url(r'^create/$', CreateNodeAPIView.as_view()), # создание нового узла
    url(r'^delete/$', DeleteNodeAPIView.as_view()), # удаление нового узла
    url(r'^all/$', NodesAPIView.as_view()),   # return all nodes
    url(r'^get_pings/$', NodesPingAPIView.as_view()),   # return ping for node
    url(r'^group/all/$', NodeGroupAPIView.as_view()),   # return all goups
    url(r'group/create/$', CreateNodeGroupAPIView.as_view()),   # create group
    url(r'group/save/$', SaveNodeGroupAPIView.as_view()),   # create group
    url(r'group/delete/$', DeleteNodeGroupAPIView.as_view()),   # create group
    url(r'srv/$', SrvPostCmdAPIView.as_view()), # send command to srv
    url(r'srv/srv_alive/$', SrvGetAliveAPIView.as_view()),# get server status (on, off)
    url(r'last_data/$', GetLastDataAPIView.as_view()),# get last ping data

]
