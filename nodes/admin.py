from django.contrib import admin
from .models import Node, NodeGroup, NodePing, NodePingSrv
# Register your models here.
admin.site.register(NodeGroup)
admin.site.register(Node)
admin.site.register(NodePing)
admin.site.register(NodePingSrv)
