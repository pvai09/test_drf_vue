# Generated by Django 3.0.6 on 2020-06-04 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodes', '0003_auto_20200509_1433'),
    ]

    operations = [
        migrations.CreateModel(
            name='NodePingSrv',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('TimeChk', models.DateTimeField()),
            ],
        ),
    ]
