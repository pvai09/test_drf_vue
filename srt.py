import time
from django.utils import timezone
from threading import Thread, Lock
import threading
import schedule
import asyncio
import django
import os
import socket
from datetime import datetime
from codex_queue import Queue
os.environ["DJANGO_SETTINGS_MODULE"] = 'test.settings'
django.setup()
from nodes.models import Node, NodePing, NodePingSrv
from nodes.settings import SRV_IP


class Server:
    nodes = []
    n_ip = []
    cmd_srv = 0
    cmd = ['/usr/bin/fping', '-c3', '-q']
    threadLock = threading.Lock()
    i = 0
    def __init__(self, ip, port):
        self.queue = Queue(ip, port)

    def start_server(self):
        self.queue.start_server()

    def stop_server(self):
        self.queue.stop_server()

    def loop(self):
        wst_list = []
        for n in Server.nodes:
            if wst_list.count(n['Wst']) == 0:
                wst_list.append(n['Wst'])
                ipl = []
                for i in Server.nodes:
                    if i['Wst'] == n['Wst']:
                        ipl.append(i['Ip'])
                self.n_ip.append({'Wst': n['Wst'], 'Ip': ipl})
        print(self.n_ip)
        threadLock = threading.Lock()
        for r in self.n_ip:
            schedule.every(int(r['Wst'])).seconds.do(self.run_threaded, self.job, r['Ip'])
        i = 0
        print("2 => ###########################")
        while True:
            time.sleep(1)
#------- Send time to db 1 time per 60 seconds
            if Server.i > 10:
                try:
                    n = NodePingSrv.objects.get(ip=SRV_IP)
#                    n.TimeChk = datetime.now(tz=None)
                    n.TimeChk = timezone.now()
                    n.save()
                except Exception as e:
                    print("Error: {}".format(e))
                    n = NodePingSrv(TimeChk = timezone.now(), ip = SRV_IP)
                    n.save(force_insert = True)
                Server.i = 0
            Server.i += 1
#-------
            if self.queue.exists():
                self.handle(self.queue.get())
            if Server.nodes:
#                print("Server.cmd_srv = ", Server.cmd_srv)
                if Server.cmd_srv == 3: #reload nodes ip
                    schedule.clear()
                    wst_list = []
                    Server.n_ip = []
                    for n in Server.nodes:
                        if wst_list.count(n['Wst']) == 0:
                            wst_list.append(n['Wst'])
                            ipl = []
                            for i in Server.nodes:
                                if i['Wst'] == n['Wst']:
                                    ipl.append(i['Ip'])
                            Server.n_ip.append({'Wst': n['Wst'], 'Ip': ipl})
                    for r in Server.n_ip:
                        schedule.every(int(r['Wst'])).seconds.do(self.run_threaded, self.job, r['Ip'])
                    Server.cmd_srv = 0
                schedule.run_pending()


    def send(self, ip, port, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))
        try:
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

    def handle(self, message):
        """
        Prototype
        """
        pass

    async def png(self, lst):
        cmd1 = self.cmd + lst
        proc = await asyncio.create_subprocess_exec(
            *cmd1,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate()
        return stderr.decode()


    def job(self, ipl):
        t_begin = timezone.now()
        d = asyncio.run(self.png(ipl))
        t_end = timezone.now()
        for l in d.split("\n"):
            pmin, pavg, pmax = 0, 0, 0
            try:
                ip, dt = l.split(":")
                ip = ip.strip()
                dt0 = dt.split(",")[0].split("=")[1]
                xmt, rcv, ploss = dt0.split("/")
                if ploss != "100%":
                    dt1 = l.split(":")[1].split(",")[1].split("=")[1]
                    pmin, pavg, pmax = dt1.split("/")
#                print("Parse {ip}: {xmt}/{rcv}/{ploss}, {pmin}/{pavg}/{pmax}".
#                       format(ip=ip, xmt=xmt, rcv=rcv, ploss=ploss,
#                              pmin=pmin, pavg=pavg, pmax=pmax))
                node = Node.objects.get(Ip=ip)
                nping = NodePing()
                nping.TimeBegin = t_begin
                nping.TimeEnd = t_end
                nping.min_ping = pmin
                nping.mid_ping = pavg
                nping.max_ping = pmax
                nping.mdev = pavg
                nping.ploss = ploss.split("%")[0]
                node.nodeping_set.add(nping, bulk=False)
            except Exception as e:
                print(e)
                pass
        self.threadLock.acquire()
        print(d)
        self.threadLock.release()


    def run_threaded(sefl, job_func, l):
        job_thread = threading.Thread(target=job_func, args=(l,))
        job_thread.start()
