import socket
import time

from codex_queue import Queue


class Server:
    nodes = []
    def __init__(self, ip, port):
        self.queue = Queue(ip, port)

    def start_server(self):
        self.queue.start_server()

    def stop_server(self):
        self.queue.stop_server()

    def loop(self):
        while True:
            time.sleep(1)
            if self.queue.exists():
                self.handle(self.queue.get())
            if self.nodes:
                print("async ping start => ", self.nodes)

    def send(self, ip, port, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))
        try:
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

    def handle(self, message):
        """
        Prototype
        """
        pass
