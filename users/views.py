from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView
from users.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework_jwt.serializers import JSONWebTokenSerializer, jwt_payload_handler
from rest_framework_jwt.settings import *
import jwt
import logging
#from rest_framework_jwt import *
from .models import User

# Create your views here.

@permission_classes([AllowAny, ])
class CreateUserAPIView(APIView):
    # Allow any user (authenticated or not) to access this url
#    permission_classes = (AllowAny,)

    def post(self, request):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

# return user id and type id
class InfoUserAPIView(APIView):
	pass

# with param: all=true : delete all user token, all=false : only this user token
class LogoutUserAPIView(APIView):
	pass

# Вход (запрос нового токена)
@permission_classes([AllowAny, ])
class SignInUserAPIView(APIView):
#        pass


#  @api_view(['POST'])
#  @permission_classes([AllowAny, ])
#  def authenticate_user(request):
   def post(self, request):
      try:
          email = request.data['login']
          password = request.data['password']
          user = User.objects.get(login=login, password=password)

#          logging.debug("-----------")
#          logging.debug(user)

          if user:
              try:
                  payload = jwt_payload_handler(user)
                  token = jwt.encode(payload, settings.SECRET_KEY)
                  user_details = {}
                  user_details['name'] = "%s %s" % (
                      user.first_name, user.last_name)
                  user_details['token'] = token
                  user_logged_in.send(sender=user.__class__,
                                      request=request, user=user)
                  return Response(user_details, status=status.HTTP_200_OK)
              except Exception as e:
                  raise e
          else:
              res = {
                  'error': 'can not authenticate with the given credentials or the account has been deactivated'}
              return Response(res, status=status.HTTP_403_FORBIDDEN)
      except KeyError:
          res = {'error': 'please provide a email and a password'}
          return Response(res)

@permission_classes([IsAuthenticated, ])
class UserRetrieveUpdateAPIView(RetrieveUpdateAPIView):

    # Allow only authenticated users to access this url
#    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer_data = request.data.get('user', {})
        print("###################")
        print(serializer_data)
        print("###################")
        serializer = UserSerializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated, ])
class InfoUserAPIView(APIView):

    # Allow only authenticated users to access this url
#    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=status.HTTP_200_OK)
