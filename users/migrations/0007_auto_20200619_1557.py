# Generated by Django 3.0.6 on 2020-06-19 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20200617_1630'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='post_code',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AddField(
            model_name='user',
            name='username',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
