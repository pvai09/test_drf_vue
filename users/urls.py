from django.conf.urls import url #, patterns
from .views import CreateUserAPIView, SignInUserAPIView, InfoUserAPIView, LogoutUserAPIView, UserRetrieveUpdateAPIView

urlpatterns = [
    url(r'^signup/$', CreateUserAPIView.as_view()), # Регистрация
    url(r'^signin/$', SignInUserAPIView.as_view()), # Вход (запрос нового токена)
    url(r'^info/$', InfoUserAPIView.as_view()),   # return user id and type id
    url(r'^logout/$', LogoutUserAPIView.as_view()), # with param: all=true = delete all user token, all=false - only this user token
    url(r'^update/$', UserRetrieveUpdateAPIView.as_view()), # with param: all=true = delete all user token, all=false - only this user token
]
