from rest_framework import serializers
from.models import User


class UserSerializer(serializers.ModelSerializer):

    date_joined = serializers.ReadOnlyField()

    class Meta(object):
        model = User
        fields = '__all__'
#        ('id', 'login', 'uid', 'email', 'phone', 'first_name', 'last_name', 'is_active', 'is_staff',
#                  'date_joined', 'password')
        extra_kwargs = {'password': {'write_only': True}}
