# users/models.py
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager
)
from django.db import transaction

class UserManager(BaseUserManager):
    @transaction.atomic
    def _create_user(self, login, uid,  password, **extra_fields): # было (self, email, ...)
        """
        Creates and saves a User with the given email,and password.
        """
#        if not login:
#            raise ValueError('The given login must be set')
        try:
            with transaction.atomic():
                user = self.model(login=login, uid=uid,   **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except:
            raise

    def create_user(self, login, uid, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(login, uid, password, **extra_fields)

    def create_superuser(self, login, uid, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(login, uid, password=password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    uid = models.CharField(max_length=5, default='email') # login is email or phone
    login = models.CharField(max_length=40, unique=True)
    email = models.EmailField(max_length=40)
    phone = PhoneNumberField(default="") # Cell_phone_max_length = 15 (ITU), ADD UNIQUE??
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    company = models.CharField(max_length=250, blank=True)
    address = models.CharField(max_length=250, blank=True)
    city = models.CharField(max_length=250, blank=True)
    country = models.CharField(max_length=250, blank=True)
    about = models.CharField(max_length=250, blank=True)
    post_code = models.CharField(max_length=250, blank=True)
    username = models.CharField(max_length=250, blank=True)


    objects = UserManager()
    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['uid']
 #   REQUIRED_FIELDS = ['uid', 'email', 'phone', 'first_name', 'last_name']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self
